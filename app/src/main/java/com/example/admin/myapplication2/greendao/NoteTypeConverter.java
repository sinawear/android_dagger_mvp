package com.example.admin.myapplication2.greendao;

import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * Created by admin on 2017/6/15.
 */

public class NoteTypeConverter implements PropertyConverter<NoteType, String> {
    @Override
    public NoteType convertToEntityProperty(String databaseValue) {
        NoteType notype = new NoteType();
        notype.str = databaseValue;
        return notype;
    }

    @Override
    public String convertToDatabaseValue(NoteType entityProperty) {
        return entityProperty.str;
    }
}