package com.example.admin.myapplication2.mvpdagger;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.admin.myapplication2.R;

import javax.inject.Inject;

public class Main3Activity extends FragmentActivity implements ConversationInteractive.View{

    @Inject
    ConversationPersenter persenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        DaggerConversationCompent.builder().conversationModule(new ConversationModule(this)).build().inject(this);

        persenter.open();
    }

    @Override
    public void openResult() {
        Log.e("log", "openResult");
    }
}
