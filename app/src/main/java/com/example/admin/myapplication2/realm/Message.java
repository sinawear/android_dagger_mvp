package com.example.admin.myapplication2.realm;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by admin on 2017/6/15.
 */

public class Message extends RealmObject{
    public String message;
    public int messageType;
    public User user;

    public FileModel file;

    public RealmList<User> members =new RealmList<>();


}
