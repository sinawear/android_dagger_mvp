package com.example.admin.myapplication2.emoji;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.admin.myapplication2.R;

import java.util.ArrayList;
import java.util.List;

public class EmojiActivity extends AppCompatActivity {

    Adapter adapter;
    List<String> listdata = new ArrayList<>();
    TextView tv_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emoji);

        tv_content = (TextView) findViewById(R.id.tv_content);


        /*
        GridView gv_content = (GridView) findViewById(R.id.gv_content);
        adapter = new Adapter();
        gv_content.setAdapter(adapter);
        */

        setEmojiToTextView();


        int unicodeJoy = 0x1F602;
        String unicodeJoy_str = "\\u1F602";
        String str = "你好";
        str += unicodeJoy;

        int pos = 0;
        pos = unicodeJoy_str.indexOf("\\u", pos);
        String result_old = unicodeJoy_str.substring(pos, pos+7);
        String result1 = unicodeJoy_str.substring(pos+2, pos+7);
        String result_new = new String(Character.toChars(Integer.valueOf(result1, 16)));
        unicodeJoy_str = unicodeJoy_str.replace(result_old, result_new);


        /*
        char[] array1 = unicodeJoy_str.toCharArray();
        for(char c : array1){
            Log.d("", ""+c);
            if()
        }
        */


        String result = "我的表情"+"\\u1F602"+"\\u1F601";
        char[] array = result.toCharArray();
        for(int i =0; i<array.length; i++){
            char c = array[i];
            Log.d("", ""+c);
        }
        tv_content.setText(result);

       // Spanned html = Html.fromHtml("\\u1F602");
       // tv_content.setText(html);


    }


    private void setEmojiToTextView(){
        int unicodeJoy = 0x1F602;
        String emojiString = getEmojiStringByUnicode(unicodeJoy);
        emojiString += "是我的表情";
        tv_content.setText(emojiString);


    }

    private String getEmojiStringByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }


    class Adapter extends BaseAdapter{

        @Override
        public int getCount() {
            return listdata.size();
        }

        @Override
        public Object getItem(int position) {
            return listdata.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {


            return null;
        }
    }
}
