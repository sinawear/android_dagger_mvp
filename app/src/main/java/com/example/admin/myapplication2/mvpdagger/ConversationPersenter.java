package com.example.admin.myapplication2.mvpdagger;

import android.util.Log;

import javax.inject.Inject;

/**
 * Created by admin on 2017/3/17.
 */

public class ConversationPersenter implements ConversationInteractive.Persenter{
    ///*
    ConversationInteractive.View view ;

    @Inject
    public ConversationPersenter(ConversationInteractive.View view){
        this.view = view;
        Log.e("log", "ConversationPersenter");
    }
    //*/




    @Override
    public void open() {
        //open
        Log.e("log", "open");
        view.openResult();
    }
}
