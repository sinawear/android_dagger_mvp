package com.example.admin.myapplication2.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by admin on 2017/3/17.
 */
@Module
public class MainModule {

    @Provides
    @Singleton
    Person getPerson(){
        Person person = new Person();
        person.name = "123";
        return person;
    }
}
