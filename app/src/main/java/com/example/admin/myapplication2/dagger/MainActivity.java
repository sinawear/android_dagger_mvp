package com.example.admin.myapplication2.dagger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.admin.myapplication2.R;
import com.example.admin.myapplication2.realm.Main7Activity;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        DaggerMainComponent.builder().mainModule(new MainModule()).build().inject(this);

        Intent intent = new Intent(this, Main7Activity.class);
        startActivity(intent);
        finish();
    }
}
