package com.example.admin.myapplication2.mvpdagger;


import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by admin on 2017/3/17.
 */
@Singleton
@Component(modules = {ConversationModule.class})
public interface ConversationCompent {
    void inject(Main3Activity activity);
}
