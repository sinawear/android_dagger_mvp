package com.example.admin.myapplication2.mvpdagger;

/**
 * Created by admin on 2017/3/17.
 */

public interface ConversationInteractive {

    public interface View{
        public void openResult();
    }


    public interface Persenter{
        public void open();
    }
}
