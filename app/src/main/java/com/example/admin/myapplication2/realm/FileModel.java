package com.example.admin.myapplication2.realm;


import io.realm.RealmObject;

public class FileModel extends RealmObject{

    public FileModelDetails file;
    public FileModelDetails thumb;

    //--
    //public transient int length;
    //public transient int width;
    //public transient int height;

    public FileModelDetails getFile() {
        return file;
    }

    public void setFile(FileModelDetails file) {
        this.file = file;
    }

    public FileModelDetails getThumb() {
        return thumb;
    }

    public void setThumb(FileModelDetails thumb) {
        this.thumb = thumb;
    }

    @Override
    public String toString() {
        return "UploadFile{" +
                "file=" + file +
                ", thumb=" + thumb +
                '}';
    }



}
