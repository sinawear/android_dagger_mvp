package com.example.admin.myapplication2.rxjava;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.example.admin.myapplication2.R;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;


public class Main4Activity extends AppCompatActivity {
    private String TAG = Main4Activity.class.getSimpleName();
    private TextView tv_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        tv_content = (TextView) findViewById(R.id.tv_content);

        // 可观察者
        Observable able = Observable.create(new Observable.OnSubscribe<String>(){

            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("hello");
                subscriber.onNext("world");
                subscriber.onCompleted();
            }
        });

        String[] array = new String[]{"hello", "world"};
        Observable able1 = Observable.from(array);

        // 观察者
        Observer<String> ob = new Observer<String>() {
            @Override
            public void onCompleted() {
                Log.e(TAG, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                Log.e(TAG, "onNext: "+s);
            }
        };


        able.subscribe(ob);


        //
        fuc();

        // 定时器
        fuc1();

        //
        fuc2();
    }

    void fuc2(){
        Observable.just("hello", "world").map(new Func1<String, Bitmap>() {
            @Override
            public Bitmap call(String s) {
                return null;
            }
        }).subscribe(new Action1<Bitmap>() {
            @Override
            public void call(Bitmap bitmap) {
                Log.e(TAG, "bitmap: "+bitmap);
            }
        });
    }

    void fuc1(){
        // time to go
        Observable.timer(1000, TimeUnit.MILLISECONDS)
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        Log.e(TAG, "call: "+aLong);
                    }
                });

        //
        Observable.interval(1000, 1000, TimeUnit.MILLISECONDS).subscribe(new Subscriber<Long>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Long aLong) {
                Log.e(TAG, "interval: "+aLong);

                if(Main4Activity.this.isFinishing())
                    this.unsubscribe();
            }
        });



    }


    void fuc(){
        Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext("hello");
                subscriber.onNext("world");
                subscriber.onCompleted();
            }
        }).subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                Log.e("fuc", "onCompleted");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                Log.e("fuc", ""+s);
            }
        });
    }

}
