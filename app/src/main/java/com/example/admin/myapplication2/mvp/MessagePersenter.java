package com.example.admin.myapplication2.mvp;

import android.util.Log;

/**
 * Created by admin on 2017/3/17.
 */

public class MessagePersenter implements MessageInteractive.Persenter{
    MessageInteractive.View view ;

    public MessagePersenter(MessageInteractive.View view){
        this.view = view;
    }


    @Override
    public void open() {
        //open
        Log.e("log", "open");
        view.openResult();
    }
}
