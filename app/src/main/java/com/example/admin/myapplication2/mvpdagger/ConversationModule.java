package com.example.admin.myapplication2.mvpdagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by admin on 2017/3/17.
 */

@Module
public class ConversationModule {

    private Main3Activity activity;

    public ConversationModule(Main3Activity activity) {
        this.activity = activity;
    }

    @Singleton
    @Provides
    public ConversationInteractive.View getView(){
        return activity;
    }

    /*
    @Singleton
    @Provides
    public ConversationInteractive.Persenter getPersenter(){
        return new ConversationPersenter(activity);
    }
    */


}
