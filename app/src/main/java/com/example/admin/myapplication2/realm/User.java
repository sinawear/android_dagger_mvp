package com.example.admin.myapplication2.realm;

import io.realm.RealmObject;

public class User extends RealmObject{

    public String userId;
    public String nickName;
    public String avatarURL;
    public String alias;
    public boolean isAdmin;
    public boolean isFriend;
    public String phoneNumber;

    public String roomID;

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatarURL='" + avatarURL + '\'' +
                ", alias='" + alias + '\'' +
                ", isAdmin='" + isAdmin + '\'' +
                ", isFriend='" + isFriend + '\'' +
                ", roomID='" + roomID + '\'' +
                '}';
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(boolean isFriend) {
        this.isFriend = isFriend;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
