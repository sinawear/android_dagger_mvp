package com.example.admin.myapplication2.dagger;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import javax.inject.Inject;

/**
 * Created by admin on 2017/3/17.
 */

public class Adapter extends BaseAdapter {

    @Inject
    public Adapter(Person person){
        Log.e("TAG", "person "+person.name);
    }


    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
