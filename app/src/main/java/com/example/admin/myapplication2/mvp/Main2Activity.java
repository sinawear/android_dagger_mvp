package com.example.admin.myapplication2.mvp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.admin.myapplication2.R;

public class Main2Activity extends AppCompatActivity implements MessageInteractive.View{

    MessagePersenter persenter = new MessagePersenter(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //
        persenter.open();

    }

    @Override
    public void openResult() {
        Log.e("log", "openresult");
    }
}
