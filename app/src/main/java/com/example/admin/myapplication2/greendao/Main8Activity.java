package com.example.admin.myapplication2.greendao;

import android.app.Activity;
import android.os.Bundle;

import com.example.admin.myapplication2.R;
import com.example.admin.myapplication2.greendao.gen.DaoMaster;
import com.example.admin.myapplication2.greendao.gen.DaoSession;
import com.example.admin.myapplication2.greendao.gen.NewMessageDao;

import java.util.List;

public class Main8Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main8);


        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(this, "message.db", null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDatabase());
        DaoSession daoSession = daoMaster.newSession();
        NewMessageDao nm = daoSession.getNewMessageDao();

        NoteType type = new NoteType();
        type.str = "sssssssssssss";

        NewMessage msg = new NewMessage();
        msg.new_message = "123";
        //msg.type = type;
        nm.insert(msg);

        List<NewMessage> userList = nm.queryBuilder()
                .build().list();

        System.out.println(""+userList);


    }
}
