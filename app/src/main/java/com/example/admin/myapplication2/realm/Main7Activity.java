package com.example.admin.myapplication2.realm;

import android.app.Activity;
import android.os.Bundle;

import com.example.admin.myapplication2.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

public class Main7Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);

        User user = new User();
        user.alias = "jack";

        User user1 = new User();
        user.alias = "ma";


        FileModelDetails fmd = new FileModelDetails();
        fmd.degree = 90;
        FileModel fm = new FileModel();
        fm.file = fmd;

        List<User> userarray = new ArrayList<>();
        userarray.add(user);
        userarray.add(user1);

        Message msg = new Message();
        msg.message = "123";
        msg.user = user;
        msg.file = fm;
        msg.members.addAll(userarray);


        /*
        // Get a Realm instance for this thread
        Realm realm = Realm.getDefaultInstance();
        // Persist your data in a transaction
        realm.beginTransaction();
        final Message msgDB = realm.copyToRealm(msg); // Persist unmanaged objects
        realm.commitTransaction();
        */

        {
            /*
            NewMessage msg1 = new NewMessage();
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            final NewMessage msgDB = realm.copyToRealm(msg1); // Persist unmanaged objects
            realm.commitTransaction();
            */


            Realm realm = Realm.getDefaultInstance();
            String str = "123";
            realm.beginTransaction();
            NewMessage2 nm2 = realm.createObject(NewMessage2.class);
            nm2.fmd = realm.createObject(FileModelDetails.class);
            nm2.fmd.degree = 90;
            nm2.new_message = str;

            User user2 = realm.createObject(User.class);
            user2.alias = "alias";
            nm2.members.add(user2);


            realm.commitTransaction();


        }


        /*
        String str = "{\n" +
                "    \"members\":[\n" +
                "        {\n" +
                "            \"userId\":\"590abc347e0f41276e7e32b3\",\n" +
                "            \"alias\":\"时帆\",\n" +
                "            \"avatarURL\":\"http://192.168.2.9:9000/group1/M00/00/06/wKgCCVkJrAKAaP6dAADEXoc4zNc187.jpg\",\n" +
                "            \"isAdmin\":true,\n" +
                "            \"isFriend\":true\n" +
                "        },\n" +
                "        {\n" +
                "            \"userId\":\"590ad8717e0f41276e7e32f5\",\n" +
                "            \"alias\":\"测试2号\",\n" +
                "            \"avatarURL\":\"http://192.168.2.9:9000/group1/M00/00/06/wKgCCVkJq32AdqdvAADEXoc4zNc032.jpg\",\n" +
                "            \"isAdmin\":false,\n" +
                "            \"isFriend\":false\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        try {
            JSONObject obj = new JSONObject(str);

            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            final Message msgDB = realm.copyToRealm(msg); // Persist unmanaged objects
            realm.commitTransaction();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        */


    }
}
