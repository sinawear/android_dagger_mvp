package com.example.admin.myapplication2.dagger;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by admin on 2017/3/17.
 */
@Singleton
@Component(modules = {MainModule.class})
public interface MainComponent {
    //Person getPerson();
    void inject(MainActivity activity);
}
