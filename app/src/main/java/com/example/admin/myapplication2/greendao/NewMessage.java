package com.example.admin.myapplication2.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by admin on 2017/6/15.
 */
@Entity
public class NewMessage extends BaseMessage{
    String new_message;

    @Generated(hash = 462527770)
    public NewMessage(String new_message) {
        this.new_message = new_message;
    }

    @Generated(hash = 1592590831)
    public NewMessage() {
    }

    /*
    @Convert(converter = NoteTypeConverter.class, columnType = String.class)
    NoteType type;
    */

    public String getNew_message() {
        return this.new_message;
    }

    public void setNew_message(String new_message) {
        this.new_message = new_message;
    }


}
